

import UIKit
//import AlamofireImage


class MenuViewController: UIViewController {

    //referencia de la imagen de perfil
    @IBOutlet weak var profileImageView: UIImageView!
    //referencia al stackview que contiene la imagen y nombre del usuario
    @IBOutlet weak var profileStackView: UIStackView!
    //refrencia al nombre del usuario
    @IBOutlet weak var nameLabel: UILabel!
    //referencia a las constraints de la imagen d eperfil
    @IBOutlet weak var imageHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var imageWidthConstraint: NSLayoutConstraint!
    
    
    
    
   static var currentTag = 1
    
   
    
    override func viewDidAppear(_ animated: Bool) {
        
      /*  if DataUser.NAME  != nil && DataUser.LAST_NAME != nil{
            self.nameLabel.text = "\(DataUser.NAME!) \(DataUser.LAST_NAME!)"
            
        }
        
        if DataUser.IMAGEURL != nil{
            let profileURL = URL(string: (DataUser.IMAGEURL))
            self.profileImageView.af_setImage(withURL: profileURL!)
        }
        
       */
    }
    
    override func viewDidLoad() {
        
        /*
        if DataUser.NAME  != nil && DataUser.LAST_NAME != nil{
            self.nameLabel.text = "\(DataUser.NAME) \(DataUser.LAST_NAME)"
            
        }
        
        if DataUser.IMAGEURL != nil{
            let profileURL = URL(string: (DataUser.IMAGEURL))
            self.profileImageView.af_setImage(withURL: profileURL!)
        }
        */
        
        super.viewDidLoad()
        
        if  UIScreen.main.bounds.height < 600{
            self.imageHeightConstraint.constant = 40
            self.imageWidthConstraint.constant = 40
            self.profileImageView.layer.cornerRadius = 20
            
        }

        
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(tapProfileView))
        
        self.profileStackView.addGestureRecognizer(tapGesture)
        
        
       

        // Do any additional setup after loading the view.
    }
    
    
    //metodo que se ejecuta al presionar la imagen o el nombre de perfil
    @objc func tapProfileView(sender: UITapGestureRecognizer) {
        
        //let vc = ProfileViewController()
       // self.navigationController?.pushViewController(vc, animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func selectViewController(_ sender: UIButton){
        
        if sender.tag == MenuViewController.currentTag{
            self.slideMenuController()?.closeLeft()
            return
        }
        
        MenuViewController.currentTag = sender.tag
        
        var vc = UIViewController();
        
        switch sender.tag {
        case 1:
            vc = HomeViewController()
            break
        case 2:
             vc = HomeViewController()
            break
        case 3:
             vc = HomeViewController()
            break
        case 4:
             vc = HomeViewController()
            break
        case 5:
            vc = HomeViewController()
            break
        case 6:
            vc = HomeViewController()
            break
        case 7:
            vc = HomeViewController()
            break
        default:
            break
        }
        
                self.slideMenuController()?.changeMainViewController(vc, close: true);
        
        
    }
    
    
    @IBAction func termsButtonAction(_ sender: UIButton) {
        
      alert(title: "¡Oops!", message: "Función no disponible actualmente.", cancel: "Cerrar");
    }
    
    
    func cleanData(){
    
        //aqui se elimina la data por que se cierra la sesion
    /*    do {
            
            try Auth.auth().signOut()
            DataUser.clean()
            
        } catch let signOutError as NSError {
            
            print ("Error signing out: %@", signOutError)
            
        }*/
        
    }
    
}
