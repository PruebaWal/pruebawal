

import UIKit
import SlideMenuControllerSwift

class ContainerSliderViewController: SlideMenuController, UINavigationControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()
        
       //cambiaos el tamaño de la barra de navegación
      SlideMenuOptions.leftViewWidth = UIScreen.main.bounds.width - UIScreen.main.bounds.width / 4
            view.backgroundColor = UIColor(red: 33/255, green: 28/255, blue: 50/255, alpha: 1)
            
            
        
        // Do any additional setup after loading the view.
    }
    
    override func awakeFromNib() {
        
        let home = HomeViewController()
        self.mainViewController = home
        
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "Left") {
            self.leftViewController = controller
        }
        
        super.awakeFromNib()
    }

}
