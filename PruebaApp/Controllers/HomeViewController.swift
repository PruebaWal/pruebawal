

import UIKit
import GoogleMaps
import CoreLocation



class HomeViewController: UIViewController ,WebBridgeDelegate,GMSMapViewDelegate{
        
    
    @IBOutlet weak var mapView: GMSMapView!
   
    //variable que controla si se esta mostrando o no la vista de la informacion de la tienda
    var stationIsShow = false
    //referencia a la view que muestra la información de la tienda
    @IBOutlet weak var infoShopsView: UIView!
    //labels de la informacion que se muestra de la tienda
    @IBOutlet weak var nameShopLabel: UILabel!
    @IBOutlet weak var adressShopLabel: UILabel!
    @IBOutlet weak var phoneShopLabel: UILabel!
    @IBOutlet weak var opensShopLabel: UILabel!
    //arreglo que contiene todas las tiendas
    var tiendas : [Tienda] = []
     //arreglo que contiene las tiendas 25 cercanas
    var tiendasCercanas : [Tienda] = []
    
    
    

    //location manager
    let locationManager = CLLocationManager()
    var locationLatitude : Double?
    var locationLongitude : Double?
    
  
   
    override func viewDidLoad() {
        super.viewDidLoad()
        
      
        
       
        //no funcionaron los servicios de ecobici
        let client_id = "1763_2mi119nx9zc408s4kss08c4gco8ookk444ogw08scgo4gwwk4o"
        let client_secret = "5ufd8a7645gkooko4cos4c8c8sck0osgo48csow8k88w8s80k8"
        WebBridge.send(type: .GET, url: "token?client_id=\(client_id)&client_secret=\(client_secret)&grant_type=client_credentials", params: [:] as NSDictionary, message: "Obteniendo", delegate: self)
        

        
    
        
        //delegados de localizacion y del mapa
         self.locationManager.delegate = self
        self.mapView.delegate = self
     
    
   
       
        
    }
    
   
  
    
    //metodo que se ejecuta al prresionar el boton de abajo a la izquierda ese lleva a la ubicacion del usuario
    @IBAction func userLocationAction(_ sender: UIButton) {
      
        
        if let latitude = self.mapView.myLocation?.coordinate.latitude{
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true
            let camera = GMSCameraPosition.camera(withLatitude: latitude, longitude:  (self.mapView.myLocation?.coordinate.longitude)!, zoom: 13)
            self.mapView.camera = camera
            
        }else{
            alert(title: "¡Aviso!", message: "Habilite la localización en ajustes para poder utilizar esta función", cancel: "OK")
        }
    }
    
    //metodo que se ejecuta al presionar el boton de abajo a la derecha para cambiar a modo detalle
    @IBAction func ShopButtonAction(_ sender: UIButton) {
       
        
        let vc = ShopViewController()
        vc.tiendas = self.tiendasCercanas
        vc.locationLatitude = self.locationLatitude
        vc.locationLongitude = self.locationLongitude
        
        MenuViewController.currentTag = 1
        self.slideMenuController()?.changeMainViewController(vc, close: true);
 
    }
    
    
//metodo que se ejecuta al presionar el boton del menu
    @IBAction func menuShow(_ sender: UIButton) {
         self.slideMenuController()?.openLeftWithVelocity(1200)

    }
    

    //metodo que se ejecuta al presionar el boton de desbloquear tienda
    @IBAction func unloackAction(_ sender: UIButton) {
        
        alert(title: "¡Oops!", message: "Función no disponible actualmente.", cancel: "Cerrar");
   
    }
    
  
  
    
    //metodos del webBridge(servicios) - no fueron requeridos aqui
    func webBridge(webbridge: WebBridge, response: Dictionary<String, AnyObject>) {
        
        
    }
    
    func webBridge(webbridge: WebBridge, response2: NSArray) {
        
    }
    
    func webBridge(webbridge: WebBridge, error: NSError) {
        
    }
    
    
    
    //metodo que carga las tiendas en pantalla
    func showShops(){
        
        
        self.tiendas = []
        
        let array = DataUser.responseLocations["responseArray"] as! NSArray
        
        for element in array{
            
        
            let dictio = element as! NSDictionary
            
                let address = dictio["address"] as! String
                let id = String(dictio["id"] as! Int)
                let lat = dictio["latPoint"] as! String
                let lng = dictio["lonPoint"] as! String
            
                let name = dictio["name"] as! String
                let phone = dictio["telephone"] as! String
                let opens = dictio["opens"] as! String
                
            let objTemp = Tienda(id: id, name: name, address: address, latitud: Double(lat)!, longitud: Double(lng)!, phone: phone,opens: opens)
                
                //ponemos la latitud y longitud del usurio como parametro
            
            
            
               
                   if self.locationLatitude != nil && self.locationLongitude != nil{
                    objTemp.userLat = self.locationLatitude!
                    objTemp.userLon = self.locationLongitude!
                    
                    let userLocation = CLLocation(latitude: objTemp.userLat, longitude: objTemp.userLon)
                    
                      print("si llega aqui y esta es la latitude \(objTemp.userLat)")
                    let stationLocation = CLLocation(latitude: objTemp.latitud, longitude: objTemp.longitud)
                    //conseguimos la distancia en metros
                    let distance = String(stationLocation.distance(from: userLocation) / 1000)
                    objTemp.distance = distance
                    
                }
                
                self.tiendas.append(objTemp)
            
            
        }
        
        tiendas.sort(by: { $0.distance.compare($1.distance) == .orderedAscending })
        
        
        //aqui ya tenemos todo el arreglo lleno de estaciones procedemos a ponerlas en el mapa
        mapView.clear()
        var count = 0
        for tienda in self.tiendas {
            
            
            let marker = GMSMarker()
            //para saber en que posicion del arreglo esta cada marca
            marker.zIndex = Int32(count)
            marker.position = CLLocationCoordinate2D(latitude: tienda.latitud, longitude: tienda.longitud)
        
             marker.icon = UIImage(named: "marker")
            
            
            marker.map = self.mapView
        
            self.tiendasCercanas.append(self.tiendas[count])
            count = count + 1
            
            
            if count >= 25{
                return;
            }
        }
        
    }
    
    
    
    
 
    
  
    
    
    
   //metodos del delegado del mapa de google
    
    
     //metodo del delegado para gestionar que se quite la vista de la informacion de la tienda si se scrollea el mapa
    func mapView(_ mapView: GMSMapView, willMove gesture: Bool) {
        //si se esta mostrando la vista de informacion de la tienda
        if stationIsShow == true{
        UIView.transition(with: view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.infoShopsView.isHidden = true
            self.stationIsShow = false
        })
            
        }
    }
    
//metodo del delegado para gestionar que hacer al presionar una marker
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
       
        // si no se esta mostrando la vista de informacion de la tienda
        if self.stationIsShow == false{
        
        self.nameShopLabel.text = self.tiendas[Int(marker.zIndex)].name
        self.adressShopLabel.text = String(self.tiendas[Int(marker.zIndex)].address)
        self.phoneShopLabel.text = String(self.tiendas[Int(marker.zIndex)].phone)
            self.opensShopLabel.text = String(self.tiendas[Int(marker.zIndex)].opens)
        
        UIView.transition(with: view, duration: 0.4, options: .transitionCrossDissolve, animations: {
            self.infoShopsView.isHidden = false
            self.stationIsShow = true
        })
            
        }else{
            //cuando clickean otro market pero pero no es necesaria la animacion porque estabas viendo uno ya
            self.nameShopLabel.text = self.tiendas[Int(marker.zIndex)].name
            self.adressShopLabel.text = String(self.tiendas[Int(marker.zIndex)].address)
            self.phoneShopLabel.text = String(self.tiendas[Int(marker.zIndex)].phone)
            self.opensShopLabel.text = String(self.tiendas[Int(marker.zIndex)].opens)
   
        }
        
        return true
    }
 

   
}



//Extension que maneja el tema de la localización en la ventana
extension HomeViewController :CLLocationManagerDelegate{
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
          
            //pedimos el permiso al usuario
            locationManager.requestWhenInUseAuthorization()
            break
        case .authorizedWhenInUse:
            print("permiso activo")
            //actualizamos ubicación y llamamos al metodo que actualiza la longitud , latitud actualiza la direccion y el mapa
           // locationManager.startUpdatingLocation()
            permissionActive()
            
            
            break
        case .authorizedAlways:
            
            
            print("permiso activo")
            
            //actualizamos ubicación
           // locationManager.startUpdatingLocation()
            permissionActive()
            
            break
        case .restricted:
            print("restringido por el momento")
           
            //Configuramos el mensaje del usuario si no tiene habilitado el permiso
            let alertController = UIAlertController(title: "Permiso Restringido", message: "Para utilizar esta sección de una mejor manera es necesario tener el permiso activado de localización,favor de ir a ajustes.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alerta) in
                let camera = GMSCameraPosition.camera(withLatitude: 4.6957173, longitude:  -74.0455271, zoom: 13)
                self.mapView.camera = camera
                
                
                self.showShops()
                
        
            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: {
            })
            
            break
        case .denied:
            print("ha sido negado el permiso")
          
            
            //Configuramos el mensaje del usuario si no tiene habilitado el permiso
            let alertController = UIAlertController(title: "Permiso Negado", message: "Para utilizar esta sección de una mejor manera es necesario tener el permiso activado de localización,favor de ir a ajustes.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alerta) in
                let camera = GMSCameraPosition.camera(withLatitude: 4.6957173, longitude:  -74.0455271, zoom: 13)
                self.mapView.camera = camera
              
                self.showShops()
         
                

            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: {
            })
            
            
            
            
            
            break
            
            
        }
        
    }
    
    
    func permissionActive(){
        
        if let location : CLLocation = locationManager.location {
            let coordinate : CLLocationCoordinate2D = location.coordinate
            
            self.locationLatitude = coordinate.latitude
            self.locationLongitude = coordinate.longitude
            
            
        
         
           
            self.showShops()
  
            
            
            self.mapView.isMyLocationEnabled = true
            self.mapView.settings.myLocationButton = true

            let camera = GMSCameraPosition.camera(withLatitude: self.locationLatitude!, longitude:  self.locationLongitude!, zoom: 13)
            self.mapView.camera = camera
             
            
            
            
            
        }else{
            
            //Configuramos el mensaje del usuario si no se pudo acceder a su ubicación por algun motivo
            let alertController = UIAlertController(title: "Ubicación no obtenida", message: "Favor de revisar su conexión a internet e intentar nuevamente.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: { (alerta) in
                let camera = GMSCameraPosition.camera(withLatitude: 4.6957173, longitude:  -74.0455271, zoom: 13)
                self.mapView.camera = camera
               
               
                self.showShops()
          
            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: {
            })
            
            
        }
        
    }
    
    
    
    
   
    
}






//extension para mandar mensaje mas facilmente con view personalizada
extension UIViewController {
    func alert(title:String, message:String, cancel:String) {
        
        
        
        
        let storyboard = UIStoryboard(name: "MenuStoryboard", bundle: nil)
        let customAlert = storyboard.instantiateViewController(withIdentifier: "CustomAlertAceptarID") as! CustomAlertViewMessage
        customAlert.providesPresentationContextTransitionStyle = true
        customAlert.definesPresentationContext = true
        customAlert.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        customAlert.modalTransitionStyle = UIModalTransitionStyle.crossDissolve
        customAlert.titulo = title
        customAlert.mensaje = message
        customAlert.delegate = self
        self.present(customAlert, animated: true, completion: nil)
        
        
        
    }
}

extension UIViewController: CustomAlertViewMessageDelegate {
    
    func okButtonTappedMessage() {
        
        
        
    }
    
    func cancelButtonTappedMessage() {
        
    }
}
