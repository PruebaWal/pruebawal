

import UIKit
import CoreLocation

class ShopViewController: UIViewController ,UITableViewDataSource{
  
    
    //referencia de la tableView
    @IBOutlet weak var tableView: UITableView!
    //arreglo que contiene todas las tiendas lo recibimos de la otra vista
    var tiendas : [Tienda] = []
 
    var locationLatitude : Double?
    var locationLongitude : Double?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

     self.tableView.register(UINib(nibName: "ShopsTableViewCell", bundle: nil), forCellReuseIdentifier: "ShopTableViewCell")
  
        //ponemos el arreglo en orden ascendente comparando la distancia del mismo
        tiendas.sort(by: { $0.distance.compare($1.distance) == .orderedAscending })
   
 
        
    }
    
   
    
    //metodo que se ejecuta al presionar el boton de abajo a la derecha el de cambio  a mapa
    @IBAction func HomeButtonAction(_ sender: UIButton) {
        let vc = HomeViewController()
        MenuViewController.currentTag = 1
        self.slideMenuController()?.changeMainViewController(vc, close: true);
    
    }
    
  
    
//metodo que se ejecuta al presionar el boton del menu
    @IBAction func menuShow(_ sender: UIButton) {
      
        self.slideMenuController()?.openLeftWithVelocity(1200)
        
        
    }
    

  
    
    //metodo que se ejecuta al presionar el boton de desbloquear Bici
    @IBAction func unloackAction(_ sender: UIButton) {
        
        alert(title: "¡Oops!", message: "Función no disponible actualmente.", cancel: "Cerrar");
        
        
    }
    

 
//metodos del datasource de la tableView
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           return tiendas.count == 0 ? 1 : tiendas.count
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        if (self.tiendas.count == 0) {
            let cell:UITableViewCell = UITableViewCell(style: .default, reuseIdentifier: "No");
            cell.textLabel?.text = "No hay tiendas disponibles";
             self.tableView.separatorStyle = .none
            return cell;
        }
        
        let cell: ShopsTableViewCell = self.tableView.dequeueReusableCell(withIdentifier: "ShopTableViewCell") as! ShopsTableViewCell
        
     cell.nameShop.text = self.tiendas[indexPath.row].name!
        cell.addressShop.text = self.tiendas[indexPath.row].address!
        
        if self.tiendas[indexPath.row].userLat != 0{
      
            
           
            let amountString = String(format: "%.01f", Double(self.tiendas[indexPath.row].distance)!)
            cell.distanceLabel.text = "\(amountString)KM"
            
        }else{
            cell.distanceLabel.text = "?¿"
        }
        
        cell.selectionStyle = .none
        return cell
        
        
    }
   
}






