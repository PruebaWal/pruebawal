

import UIKit

class ShopsTableViewCell: UITableViewCell {

   
    @IBOutlet weak var nameShop: UILabel!
   
    @IBOutlet weak var addressShop: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    
}
