import UIKit
import SystemConfiguration
import Alamofire

protocol WebBridgeDelegate {
    func webBridge(webbridge:WebBridge, response:Dictionary<String, AnyObject>)
    func webBridge(webbridge:WebBridge, response2:NSArray)
    func webBridge(webbridge:WebBridge, error:NSError)
}

enum WebBridgeMethod {
    case GET
    case PUT
    case POST
    case DELETE
    case PATCH
}

class WebBridge: NSObject {
    
    static var manager: SessionManager!;
    static var DEBUG_MODE:Bool = true;
    
    var timer:Timer!;
    var delegate:WebBridgeDelegate!;
    var endpoint:String;
    var loader:Bool;
    var params:NSDictionary!;
    var message:String!;
    var method:WebBridgeMethod!;
    
    init(url:String!, delegate:WebBridgeDelegate!) {
        self.delegate = delegate;
        self.endpoint      = url;
        self.loader   = false;
        super.init();
    }
    
    
    /*-----------------*/
    /* CLASS FUNCTIONS */
    
    class func builtUrl(url:String) -> String {
        return url.hasPrefix("https://") ? url : "https://pubsbapi.smartbike.com/oauth/v2/" + url;
    }
    
    class func encode(params:NSDictionary) -> String {
        
        var data:Array<String> = [];
        for (key, value) in params {
            data.append("\(key)=\(value)");
        }
        return data.joined(separator: "&");
    }
    
    @discardableResult
    class func send(type:WebBridgeMethod, url:String, params:NSDictionary!, message:String!, delegate:WebBridgeDelegate!) -> WebBridge! {
        
        let bridge = WebBridge(url: url, delegate: delegate);
        bridge.send(method: type, url:url, params: params, message: message, delegate: delegate);
        
        return bridge;
    }
    
    class func connectedToNetwork() -> Bool {
        
        var zeroAddress = sockaddr_in()
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        
        guard let defaultRouteReachability = withUnsafePointer(to: &zeroAddress, {
            
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                
                SCNetworkReachabilityCreateWithAddress(nil, $0)
                
            }
            
        }) else {
            
            return false
        }
        
        
        var flags : SCNetworkReachabilityFlags = []
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability, &flags) {
            return false
        }
        
        let isReachable = flags.contains(.reachable)
        let needsConnection = flags.contains(.connectionRequired)
        return (isReachable && !needsConnection)
    }
    
    class func log(msg:AnyObject!) {
        if (DEBUG_MODE) {
            print(msg);
        }
    }
    
    
    
    /*------------------*/
    /* MEMBER FUNCTIONS */
    func send(method:WebBridgeMethod, url:String, params:NSDictionary!, message:String!, delegate:WebBridgeDelegate!) {
        
        if (!WebBridge.connectedToNetwork()) {
            
            /*
             let app = UIApplication.sharedApplication().delegate as! AppDelegate
             let rvc = app.window!.rootViewController;
             UIAlertView(title: "Error", message: "Parece que no tienes conexión a internet. Por favor activa tu WiFi o conéctate a la red celular", delegate: nil, cancelButtonTitle: "Cerrar").show();
             */
            
            return;
        }
        
        let config:URLSessionConfiguration = URLSessionConfiguration.default;
        let builtUrl:String = WebBridge.builtUrl(url: url);
        
        self.params  = params;
        self.message = message;
        self.method  = method;
        
        if (WebBridge.manager == nil) {
            WebBridge.manager = Alamofire.SessionManager(configuration: config);
        }
        
        if (message != nil) {
            
            loader = true;
            
            var config:LoaderView.Config = LoaderView.Config()
            config.size = 100;
            config.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.8);
            config.spinnerColor    = UIColor.white;
            config.titleTextColor  = UIColor.white;
            LoaderView.setConfig(config: config);
            
            LoaderView.show(title: message, animated: true)
            
        }
        
        WebBridge.log(msg: "-------------------------" as AnyObject);
        
        
        let headers:Dictionary<String, String> = Dictionary();
       /* if (DataUser.TOKEN != nil) {
            headers["Authorization"] = "Bearer " + DataUser.TOKEN!;
        }*/
        
        
        /* INICIA MÉTODO GET */
        if (method == .GET) {
            
            var query = "";
            if (params != nil) {
                let p = WebBridge.encode(params: params);
                query = builtUrl.contains("?") ? ("&" + p) : ("?" + p);
            }
            
            self.endpoint = endpoint + query;
            
            WebBridge.log(msg: "GET: \(builtUrl + query)" as AnyObject);
            WebBridge.log(msg: headers as AnyObject);
            
            WebBridge.manager.request(builtUrl + query, method: HTTPMethod.get, parameters: nil, encoding: JSONEncoding.default, headers: headers).responseString(completionHandler: { response in
                switch response.result {
                case .success:
                    let data = response.result.value!.data(using: String.Encoding.utf8)!;
                    
                    do{
                        if let jsonObect = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary{
                            
                            self.success(json: jsonObect)
                            
                        } else if let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                            
                            self.successArray(json: jsonArray)
                            
                        }else{
                            WebBridge.log(msg: "\(response.result.value!)" as AnyObject);
                        }
                    }catch{
                        WebBridge.log(msg: "\(response.result.value!)" as AnyObject);
                        self.failure(key: "JSON", message:response.result.value!);
                    }
                    
                //case .failure(ler error):
                    
                case .failure(let error):
                    WebBridge.log(msg: error as AnyObject);
                    self.failure(key: "Error", message:error.localizedDescription);
                }
            })
            
            
        } else {
            
            /* INICIA MÉTODO POST */
            
            //var paramsp:Dictionary<String,AnyObject> = Dictionary();
            //var paramsd:Dictionary<String,AnyObject> = Dictionary();
            
            var m = Alamofire.HTTPMethod.post;
            if (method == WebBridgeMethod.PUT) {
                m = Alamofire.HTTPMethod.put;
            } else if (method == WebBridgeMethod.DELETE) {
                m = Alamofire.HTTPMethod.delete;
            }
            
            WebBridge.log(msg: "\(m): \(builtUrl)" as AnyObject);
            WebBridge.log(msg: "HEADER: \(headers)" as AnyObject);
            WebBridge.log(msg: "PARAMETERS:" as AnyObject);
    
            
            var paramsp:Dictionary<String,AnyObject> = Dictionary();
            var paramsd:Dictionary<String,AnyObject> = Dictionary();
            
            for (key, value) in params {
            
                if value is Dictionary<String, AnyObject>{
                 
                    paramsd[key as! String] = value as AnyObject?;
                
                }else{
                
                    print(" - \(m): \(key) : \(value)");
                    paramsp[key as! String] = value as AnyObject?;

                }
                
                
            }
        
            
            WebBridge.manager.upload(multipartFormData: { multipartFormData in
                
                for (key, value) in paramsd {
                    let file:NSDictionary = value as! NSDictionary;
                    
                    multipartFormData.append(file["file"] as! Data, withName: key, fileName: file["name"] as! String, mimeType: file["mime"] as! String)
                    
                }
                
                for (key, value) in paramsp {
                    
                    multipartFormData.append(value.data(using: String.Encoding.utf8.rawValue)!, withName: key)
                }
                
                
            }, to: builtUrl, method: m, headers: headers, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .success(let upload, _, _):
                upload.responseString { response in switch response.result {
                
                case .success(let JSONSTR):
                    
                    let data = JSONSTR.data(using: String.Encoding.utf8)!;
                    
                    /*do {
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary;
                        self.success(json: json);
                    } catch {
                        print("\(JSONSTR)");
                        self.failure(key: "JSON", message:JSONSTR);
                    }*/
                    
                    do{
                        if let jsonObect = try JSONSerialization.jsonObject(with: data, options: []) as? NSDictionary{
                            
                            self.success(json: jsonObect)
                            
                        } else if let jsonArray = try JSONSerialization.jsonObject(with: data, options: []) as? NSArray {
                            
                            self.successArray(json: jsonArray)
                            
                        }else{
                            WebBridge.log(msg: "\(response.result.value!)" as AnyObject);
                        }
                    }catch{
                        WebBridge.log(msg: "\(response.result.value!)" as AnyObject);
                        self.failure(key: "JSON", message:response.result.value!);
                    }
                    
                case .failure(let error):
                    self.failure(key: "Error", message:error.localizedDescription);
                
                    }
                
                    }
                case .failure(let encodingError):
                print("ERROR ENCODING: \(encodingError)");
                self.failure(key: "Encoding", message:"Error encoding image");
                }
                })
            
            print("-------------------------");
            
        }
        
        WebBridge.log(msg: "-------------------------" as AnyObject);
        
    }
    
    
    
    /*-----------------*/
    /* MEMBER RESPONSE */
    
    func success(json:NSDictionary) {
        
        WebBridge.log(msg: "RESPONSE j: \(json)" as AnyObject);
        
        if (delegate != nil) {
            //delegate?.webBridge(self, response: json as! Dictionary<String, AnyObject>);
            delegate?.webBridge(webbridge: self, response: json as! Dictionary<String, AnyObject>)
        }
        if (loader) {
            LoaderView.hide();
        }
        
    }
    
    func successArray(json:NSArray) {
        
        WebBridge.log(msg: "RESPONSE a: \(json)" as AnyObject);
        
        if (delegate != nil) {
            delegate?.webBridge(webbridge: self, response2: json)
        }
        if (loader) {
            LoaderView.hide();
        }
        
    }
    
    
    func failure(key:String, message:String) {
        
        let userInfo: [NSObject : AnyObject] = [ NSLocalizedDescriptionKey as NSObject :  NSLocalizedString("Error", value: message, comment: "") as AnyObject, ]
        let error = NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: (userInfo as! [String : Any]))
        
        if (loader) {
            LoaderView.hide();
        }
        
        print("---------------------");
        print("ERROR URL \(self.endpoint)");
        print("ERROR: \(message)");
        print("---------------------");
        //WebBridge.log("ERROR: \(message)");
        
        if (delegate != nil) {
            delegate?.webBridge(webbridge: self, error: error)
        }
        
    }
    
}

